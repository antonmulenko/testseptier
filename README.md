#### Paths
Для просмотра карты http://localhost:3000/  
Для редактирования координат http://localhost:3000/admin

#### Run both (client and server)
Запуск в параллельном режиме клиентской и серверной частей  
gradle для обоих частей загрузится автоматически
```bash
testseptier$ ./gradlew run
```

#### Run client
Запуск клиентской части  
```bash
testseptier$ ./gradlew :client:run
```
или
```bash
testseptier/client$ npm start
```

#### Run server
Запуск серверной части  
```bash
testseptier$ ./gradlew :server:run
```

#### Clean
Очистка проекта  
```bash
testseptier$ ./gradlew clean
```

#### Clean process
Очистка проекта  
```bash
testseptier$ killall java node
```