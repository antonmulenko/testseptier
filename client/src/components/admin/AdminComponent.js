import React from "react";
import sendAddMany from "../../service/HttpRequestService";
import message from '../../model/message.json';

class AdminComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = { // default values only for show in test mode
            lat: 32.097839,
            lon: 34.846317,
            blockUi: false
        }
    }

    onLatChange = (e) => {
        //todo make validate
        this.setState({lat: e.target.value})
    };

    onLonChange = (e) => {
        //todo make validate
        this.setState({lon: e.target.value})
    };

    onClickButton = (e) => {
        this.setState({blockUi: true});

        message.GpsExt.Lat = this.state.lat;
        message.GpsExt.Lon = this.state.lon;

        sendAddMany(message, function () {
            this.setState({blockUi: false});
        }.bind(this));
    };

    render() {
        const {lat, lon, blockUi} = this.state;
        return (
            <div>
                <h1>Admin</h1>
                <label style={{display: "block"}}>
                    Lat:
                    <input key="latInput" type="text" value={lat} onChange={this.onLatChange} disabled={blockUi}/>
                </label>
                <label style={{display: "block"}}>
                    Lon:
                    <input key="lonInput" type="text" value={lon} onChange={this.onLonChange} disabled={blockUi}/>
                </label>

                <input key="inputButton" value="Submit" type="button" onClick={this.onClickButton} disabled={blockUi}/>
            </div>
        )
    }
}

export default AdminComponent;