import React from "react";

class ServiceDataComponent extends React.Component {
    render() {
        const {lon, lat, snr, rssi} = this.props;
        return (
            <div>
                <div>
                    <table>
                        <tbody>
                        <tr key="tr0">
                            <td key="td00">
                                Lat: {lat}
                            </td>
                            <td key="td01">
                                Lon: {lon}
                            </td>
                        </tr>
                        <tr key="tr1">
                            <td key="td10">
                                SNR: {snr}
                            </td>
                            <td key="td11">
                                RSSI: {rssi}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default ServiceDataComponent;