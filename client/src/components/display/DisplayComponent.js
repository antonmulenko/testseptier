import React from "react";
import ServiceDataComponent from "./serviceData/ServiceDataComponent";
import MapComponent from "./map/MapComponent";
import initWebSocket from "../../service/WebSocketService";

class DisplayComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            lon: 0,
            lat: 0,
            snr: 0,
            rssi: 0,
            command: 0
        };

        initWebSocket(this.onOpen, this.onMessage);
    }

    onOpen = () => {
        console.log("connect to server successfully")
    };

    onMessage = (message) => {
        let data = JSON.parse(message.data);

        this.setState({
            command: data.Command,
            lat: data.GpsExt.Lat,
            lon: data.GpsExt.Lon,
            snr: data.Points[0].SNR,
            rssi: data.Points[0].RSSI,
        });
    };

    render() {
        const {lon, lat, snr, rssi, command} = this.state;
        return (
            <div>
                {command === 0 ? (
                    [
                        <ServiceDataComponent key={0} lat={lat} lon={lon} snr={snr} rssi={rssi}/>,
                        <MapComponent key={1} lat={lat} lon={lon}/>
                    ]
                ) : (
                    <div key={0}>
                        <h4>Show content for other command</h4>
                    </div>
                )}
            </div>
        )
    }
}

export default DisplayComponent;