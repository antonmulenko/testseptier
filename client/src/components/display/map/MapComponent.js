import React from "react";
import MapGL, {Marker} from 'react-map-gl';

class MapComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            viewport: {
                latitude: 32,
                longitude: 35,
                zoom: 10,
                bearing: 0,
                pitch: 0,
                width: '100 %',
                height: 500,
            }
        }
    }

    render() {
        const {viewport} = this.state;
        const {lon, lat} = this.props;

        return (
            <MapGL
                {...viewport}
                onViewportChange={(viewport) => this.setState({viewport})}
                mapStyle="mapbox://styles/mapbox/light-v10"
                mapboxApiAccessToken="pk.eyJ1IjoidG9uaWNteCIsImEiOiJjazc5amZsdGgwcThoM2ZwanpuZXdkcG9kIn0.PznCOcU7t0HuHRT9XenT6Q">
                <Marker longitude={lon} latitude={lat}>
                    <img src="https://img.icons8.com/android/24/000000/marker.png" alt="" className="marker"/>
                </Marker>
            </MapGL>
        )
    }
}

export default MapComponent;