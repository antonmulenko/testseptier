const ADD_MANY_ENDPOINT = "/addmany";

function sendToAddMany(messageObj, callback) {
    fetch(ADD_MANY_ENDPOINT, {
        method: 'post',
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8',
        },

        body: JSON.stringify(messageObj)
    }).then(() => callback());
}

export default sendToAddMany;