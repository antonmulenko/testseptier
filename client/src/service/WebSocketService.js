import ReconnectingWebSocket from "reconnecting-websocket";

const WEB_SOCKET_ENDPOINT = "ws://localhost:8080/topic";

function init(openCallback, onMessageCallback) {
    let ws = new ReconnectingWebSocket(WEB_SOCKET_ENDPOINT);
    ws.addEventListener('open', openCallback);
    ws.addEventListener('message', onMessageCallback);
}

export default init;