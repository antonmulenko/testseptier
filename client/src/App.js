import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import DisplayComponent from "./components/display/DisplayComponent";
import AdminComponent from "./components/admin/AdminComponent";

class App extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={DisplayComponent}/>
                    <Route path="/admin" component={AdminComponent}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
