package com.example.server.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AddManyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void postAddManyPositive() throws Exception {
        String postValue = "{\"DftrxId\":\"dftrx0\",\"Config\":{\"RxGain\":19,\"Network\":0,\"2g_params\":{\"Arfcn\":540,\"Ulsc\":65,\"Dlsc\":2},\"3g_params\":{\"Arfcn\":10788,\"Ulsc\":8758542,\"Dlsc\":369},\"4g_params\":{\"Arfcn\":1400,\"Ulsc\":5,\"Dlsc\":0},\"Alpha\":0.017,\"Slot\":\"0\",\"Celltrack\":0,\"Watcher\":0,\"Antenna\":0,\"GpsSrc\":1,\"Version\":2,\"App\":1,\"GsmMode\":1,\"Url\":\"http://localhost:8081/addmany\",\"Sound\":1},\"State\":{\"DlMode\":0,\"Tune\":\"ul\",\"SignalFound\":0,\"Hsdpa\":0,\"Standard\":\"DCS\",\"Usrp\":1,\"Capabilities\":16678719,\"Expiration\":1607713200000,\"Compression\":1,\"Error\":0,\"Remote\":0},\"GpsExt\":{\"Status\":0,\"Lat\":\"32.037839\",\"Lon\":\"34.906317\",\"Alt\":52.6,\"TS\":138},\"Compass\":{\"Hdg\":0,\"FitErr\":\"\"},\"Timestamp\":0,\"Points\":[{\"Id\":552,\"SettingsVersion\":0,\"Channel\":65,\"SNR\":33.2,\"Angles\":[{\"TSi\":139,\"TSf\":0.259143384615,\"RSSI_m\":-28.3,\"RSSI_s\":0,\"SNR_m\":35.9,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.54462,\"Int_s\":0,\"Int\":-3.54462,\"Ant\":0,\"RxGain\":19,\"V\":1},{\"TSi\":139,\"TSf\":0.263758769231,\"RSSI_m\":-28.3,\"RSSI_s\":0,\"SNR_m\":37.4,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.61846,\"Int_s\":0,\"Int\":-3.61846,\"Ant\":0,\"RxGain\":19,\"V\":1},{\"TSi\":139,\"TSf\":0.268374153846,\"RSSI_m\":-28.4,\"RSSI_s\":0,\"SNR_m\":35.9,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.54462,\"Int_s\":0,\"Int\":-3.54462,\"Ant\":0,\"RxGain\":19,\"V\":1},{\"TSi\":139,\"TSf\":0.342220307692,\"RSSI_m\":-28.3,\"RSSI_s\":0,\"SNR_m\":37.4,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.61846,\"Int_s\":0,\"Int\":-3.61846,\"Ant\":0,\"RxGain\":19,\"V\":1},{\"TSi\":139,\"TSf\":0.346835692308,\"RSSI_m\":-28.3,\"RSSI_s\":0,\"SNR_m\":40.5,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.61846,\"Int_s\":0,\"Int\":-3.61846,\"Ant\":0,\"RxGain\":19,\"V\":1},{\"TSi\":139,\"TSf\":0.351451076923,\"RSSI_m\":-28.3,\"RSSI_s\":0,\"SNR_m\":33.2,\"SNR_s\":0,\"Master\":1,\"An\":0,\"Phase\":0,\"Visible\":1,\"Int_m\":-3.61846,\"Int_s\":0,\"Int\":-3.61846,\"Ant\":0,\"RxGain\":19,\"V\":1}],\"RSSI\":-28.3,\"Angle2a\":0,\"Antenna2a\":22652576,\"Compression\":1,\"Antenna\":22652576,\"TSi\":139,\"TSf\":0.351451076923,\"Clocks\":[0,0,1536665497356427,1536665497398082]}]}";

        MvcResult storyResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/addmany")
                .contentType(MediaType.TEXT_PLAIN)
                .content(postValue))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }
}