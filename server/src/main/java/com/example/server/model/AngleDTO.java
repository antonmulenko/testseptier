package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AngleDTO {
    @JsonProperty("TSi")
    private int tsi;

    @JsonProperty("TSf")
    private float tsf;

    @JsonProperty("RSSI_m")
    private float rssim;

    @JsonProperty("RSSI_s")
    private float rssis;

    @JsonProperty("SNR_m")
    private float snrm;

    @JsonProperty("SNR_s")
    private float snrs;

    @JsonProperty("Master")
    private int master;

    @JsonProperty("An")
    private float an;

    @JsonProperty("Phase")
    private float phase;

    @JsonProperty("Visible")
    private int visible;

    @JsonProperty("Int_m")
    private float intm;

    @JsonProperty("Int_s")
    private float ints;

    @JsonProperty("Int")
    private float intValue;

    @JsonProperty("Ant")
    private int ant;

    @JsonProperty("RxGain")
    private int rxgain;

    @JsonProperty("V")
    private int v;
}
