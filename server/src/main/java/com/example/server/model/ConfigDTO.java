package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfigDTO {
    @JsonProperty("RxGain")
    private int rxgain;

    @JsonProperty("Network")
    private int network;

    @JsonProperty("2g_params")
    private ParamsDTO params2g;

    @JsonProperty("3g_params")
    private ParamsDTO params3g;

    @JsonProperty("4g_params")
    private ParamsDTO params4g;

    @JsonProperty("Alpha")
    private float alpha;

    @JsonProperty("Slot")
    private String slot;

    @JsonProperty("Celltrack")
    private int celltrack;

    @JsonProperty("Watcher")
    private int watcher;

    @JsonProperty("Antenna")
    private int antenna;

    @JsonProperty("GpsSrc")
    private int gpssrc;

    @JsonProperty("Version")
    private int version;

    @JsonProperty("App")
    private int app;

    @JsonProperty("GsmMode")
    private int gsmmode;

    @JsonProperty("Url")
    private String url;

    @JsonProperty("Sound")
    private float sound;

    @JsonProperty("Mode")
    private int mode;

    @JsonProperty("ScanPSC")
    private String scanpsc;

    @JsonProperty("ScanULSC")
    private String scanulsc;

    @JsonProperty("ScanTimeouts")
    private String scantimeouts;

    public void setWatcher(int watcher) {
        this.watcher = watcher;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setScanpsc(String scanpsc) {
        this.scanpsc = scanpsc;
    }

    public void setScanulsc(String scanulsc) {
        this.scanulsc = scanulsc;
    }

    public void setScantimeouts(String scantimeouts) {
        this.scantimeouts = scantimeouts;
    }
}
