package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MessageDTO {

    @JsonProperty("DftrxId")
    private String dftrxid;

    @JsonProperty("Config")
    private ConfigDTO config;

    @JsonProperty("State")
    private StateDTO state;

    @JsonProperty("GpsExt")
    private GpsExtDTO gpsext;

    @JsonProperty("Compass")
    private CompassDTO compass;

    @JsonProperty("Timestamp")
    private int timestamp;

    @JsonProperty("Points")
    private List<PointDTO> points;

    @JsonProperty("Command")
    private int command;

    public void setCommand(int command) {
        this.command = command;
    }

    public ConfigDTO getConfig() {
        return config;
    }
}
