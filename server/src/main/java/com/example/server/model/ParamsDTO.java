package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParamsDTO {
    @JsonProperty("Arfcn")
    private int arfcn;

    @JsonProperty("Ulsc")
    private int ulsc;

    @JsonProperty("Dlsc")
    private int dlsc;
}
