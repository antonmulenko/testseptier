package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompassDTO {
    @JsonProperty("Hdg")
    private float hdg;

    @JsonProperty("FitErr")
    private String fiterr;
}
