package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GpsExtDTO {

    @JsonProperty("Status")
    private int status;

    @JsonProperty("Lat")
    private float lat;

    @JsonProperty("Lon")
    private float lon;

    @JsonProperty("Alt")
    private float alt;

    @JsonProperty("TS")
    private int ts;
}
