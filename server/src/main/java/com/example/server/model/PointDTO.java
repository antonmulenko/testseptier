package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PointDTO {
    @JsonProperty("Id")
    private int id;

    @JsonProperty("SettingsVersion")
    private int settingsversion;

    @JsonProperty("Channel")
    private int channel;

    @JsonProperty("SNR")
    private float snr;

    @JsonProperty("Angles")
    private List<AngleDTO> angles;

    @JsonProperty("RSSI")
    private float rssi;

    @JsonProperty("Angle2a")
    private float angle2a;

    @JsonProperty("Antenna2a")
    private int antenna2a;

    @JsonProperty("Compression")
    private int compression;

    @JsonProperty("Antenna")
    private int antenna;

    @JsonProperty("TSi")
    private int tsi;

    @JsonProperty("TSf")
    private float tsf;

    @JsonProperty("Clocks")
    private List<Long> clocks;
}
