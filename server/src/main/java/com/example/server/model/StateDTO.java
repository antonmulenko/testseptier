package com.example.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StateDTO {
    @JsonProperty("DlMode")
    private int dlmode;

    @JsonProperty("Tune")
    private String tune;

    @JsonProperty("SignalFound")
    private int signalfound;

    @JsonProperty("Hsdpa")
    private int hsdpa;

    @JsonProperty("Standard")
    private String standard;

    @JsonProperty("Usrp")
    private int usrp;

    @JsonProperty("Capabilities")
    private int capabilities;

    @JsonProperty("Expiration")
    private long expiration;

    @JsonProperty("Compression")
    private int compression;

    @JsonProperty("Error")
    private int error;

    @JsonProperty("Remote")
    private int remote;
}
