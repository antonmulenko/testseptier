package com.example.server.handlers;

import com.example.server.model.MessageDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WebSocketHandler extends TextWebSocketHandler {

    Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);

    private Set<WebSocketSession> sessions = ConcurrentHashMap.newKeySet();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
    }

    public void sendBroadcastMessage(MessageDTO messageDto) {
        ObjectMapper objectMapper = new ObjectMapper();
        String stringMessage;
        try {
            stringMessage = objectMapper.writeValueAsString(messageDto);
        } catch (JsonProcessingException e) {
            logger.error("error convert MessageDTO to string", e);
            return;
        }

        sessions.forEach(session -> {
            try {
                session.sendMessage(new TextMessage(stringMessage));
            } catch (IOException e) {
                logger.error("cannot sent message to session", e);
            }
        });
    }
}
