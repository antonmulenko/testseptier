package com.example.server.controllers;

import com.example.server.handlers.WebSocketHandler;
import com.example.server.model.MessageDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/addmany")
public class AddManyController {

    Logger logger = LoggerFactory.getLogger(AddManyController.class);

    @Autowired
    private WebSocketHandler webSocketHandler;

    @PostMapping
    public void postAddMany(HttpServletResponse response, @RequestBody String jsonString) {
        ObjectMapper om = new ObjectMapper();
        MessageDTO messageDto;
        try {
            messageDto = om.readValue(jsonString, MessageDTO.class);
        } catch (JsonProcessingException e) {
            logger.error("error parsing string to MessageDTO", e);
            return;
        }

        messageDto.setCommand(0);
        messageDto.getConfig().setWatcher(0);
        messageDto.getConfig().setScanpsc(" ");
        messageDto.getConfig().setScanulsc(" ");
        messageDto.getConfig().setScantimeouts(" ");
        messageDto.getConfig().setMode(0);
        webSocketHandler.sendBroadcastMessage(messageDto);
    }
}
